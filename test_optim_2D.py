# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 13:21:16 2024

@author: tobia
"""

import scipy.optimize as sop
import numpy as np
from test_few_spins_2D import MySystem
 
a = 2                               #lattice constant, arbitrairy length
N = 100                             #number of spins
                      
J = .5                               #magnitude, exchange interaction between nearest-neigh's
K = 0.1                             #magnitnude, (magneto-crystalline) anisotropy
dmi_mag = 0.5                         #magnitude of DMI interaction
dmi = dmi_mag*np.array([0,1,0])     #vector, orientation of DMI
easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)

I = MySystem(N, a, J, K, dmi, easy_ax)
I.Setup_2D(set_type= "1_in_FM") #Setup 1 downspin in a chain of upspins



x0 = I.s0       #initial config of spins, defined by Setup_1D
x0[2] +=0.001   #would be trapped in local minima
print("Cartesian coordinates of the initial config: \n")
print(np.round(I.sph_to_car2D(x0)),"\n")

ret = sop.minimize(I.Hamiltonian_pbc, x0, tol = 1e-6)

I.plot_config(ret.x)   
    

    
#print("E(initial config)\n" ,I.Hamiltonian_pbc(I.spins))
#I.plot_config(I.spins) #If there was no change between initial and current positions -> plot only initial configuration


#dr = np.reshape(dr,(self.N,self.N,3))
#self.force=np.sum( np.multiply(pref[:,:,None],dr),axis=0)

I2 = MySystem(N, a, J, K, dmi, easy_ax)
I2.Setup_2D(set_type= "FM") #Setup all spins up



x0 = I.s0       #initial config of spins, defined by Setup_1D
x0[1] -=0.02   #would be trapped in local minima
print("Cartesian coordinates of the initial config: \n")
print(np.round(I.sph_to_car2D(x0)),"\n")
print("E(initial config)\n" ,I.Hamiltonian_pbc(x0))
print("E(FM)\n" ,I2.Hamiltonian_pbc(I2.s0))
ret = sop.minimize(I.Hamiltonian_pbc, x0, tol = 1e-3)
#print(ret)

#I.plot_config(ret.x)   

''' 
a = 2
N = 5
J = 0.5
K = 0.1
dmi_mag = 0
dmi = dmi_mag*np.array([0,1,0])
easy_ax = np.array([1,0,0]) #test different easy axis!


I = MySystem(N, a, J, K, dmi, easy_ax)
I.Setup_2D(set_type= "FM")

#Here I do not minimize (find relaxed config), since FM should already be the relaxed config (try minimize this config to check?)

####
print("E(initial config)\n" ,I.Hamiltonian_pbc(I.spins))
FM = I.spins

#I.plot_config(I.spins) #If there was no change between initial and current positions -> plot only initial configuration
'''



                    
        
        