import numpy as np

class MySystem:

    def __init__(self,Tinit,dt,density,N,nsteps):
        np.random.seed(10)
        self.nc = int(np.round((N/4)**(1/3)))
        self.N = 4*self.nc**3
        self.pos = np.zeros((4*(self.nc)**3, 3))
        self.volume = self.N/density
        self.vel = (np.random.rand(self.N,3)-0.5)*2
        self.epot = np.zeros(nsteps + 1)
        self.ekin = np.zeros(nsteps + 1)
        self.Tinit = Tinit
        self.dt = dt
        #self.nsteps = nsteps
        self.L = self.volume**(1/3)
        
        print("There are now",self.N,"atoms in the system.")
        self.virial = 0
        self.pres = np.zeros(nsteps + 1)
        self.iter = 0
        

        
    def fccmake(self):
        r = np.array([[0.0, 0.0, 0.0], [0.5, 0.5, 0.0], [0.0, 0.5, 0.5], [0.5, 0.0, 0.5]])
        

        n = 0
        for k in range(0,self.nc):
            for l in range(0,self.nc):
                 for m in range(0,self.nc):
                    for ii in range(0,4):
                        self.pos[n][0] = (r[ii,0] + k)/(self.nc)
                        self.pos[n][1] = (r[ii,1] + l)/(self.nc)
                        self.pos[n][2] = (r[ii,2] + m)/(self.nc)
                        n=n+1
        self.pos = self.L*self.pos
        

    def velocity_generate(self):
        v_avg = sum(self.vel)/self.N

        #adjust for total average velocity to be zero
        for ii in range(self.N):
            self.vel[ii,:] = self.vel[ii,:] -v_avg
            #self.vel[ii][1] = self.vel[ii][1] -v_avg
            #self.vel[ii][2] = self.vel[ii][2] -v_avg
        #renormalize velocities to initial temperature
        ekin = np.sum(np.square(self.vel))/2
        
        actual_temp = 2*ekin/(3*self.N)
        print(actual_temp)
        self.vel = self.vel*(self.Tinit/actual_temp)**(1/2)


    def LJ_pbc(self,nter):
        rc = self.L/2
        poten = 0
        self.virial = 0
        self.force = np.zeros((self.N,3))
        
        mask= np.ones((self.N,self.N),dtype=bool)
        np.fill_diagonal(mask,False)
        
        dr = self.pos-self.pos[:,None]
        print(dr)
        #minimum image convention
        dr = dr - self.L*np.round(dr/self.L)

        dist = (np.sum(np.square(dr),axis = 2)**(0.5))
        
        rinv = np.reciprocal(dist,where=mask)

        mask[np.sum(np.square(dr),axis = 2)**2 >= rc] = False
        mask = np.reshape(mask,(self.N,1,self.N))
        mask = np.reshape(np.concatenate((mask,mask,mask)),(self.N,3,self.N))
        
        
        wij = 24*(2*rinv**12 - rinv**6) #stimmt überein
        
        poten = 4*(rinv**12-rinv**6)
        self.epot[nter] = np.sum(poten)/self.N/2
        self.virial = np.sum(wij)/2
        pref = np.multiply(wij,np.square(rinv)) #stimmt auch
        dr = np.reshape(dr,(self.N,self.N,3))
        self.force=np.sum( np.multiply(pref[:,:,None],dr),axis=0)
        """
        if nter == 1:
            print(self.force)
        """
        """
        for ii in range(self.N-1):
            for jj in range(ii+1,self.N):
                rij = self.pos[ii,:] - self.pos[jj,:]
                
                #minimum image convention
                rij = rij - self.L*np.round(rij/self.L)
                dist = np.sum(np.square(rij))**(0.5)

                if dist < rc:
                    r6 = 1/dist**6
                    r12 = r6**2

                    poten = poten + 4*(r12 -r6)
                    #virial
                    wij = 24*(2*r12-r6)
                    
                    self.virial = self.virial + wij
                    #force
                    f = wij/(dist**2)*rij
                    self.force[ii,:] = self.force[ii,:] + f
                    self.force[jj,:] = self.force[jj,:] - f
        
        self.epot[nter] = poten/self.N
        """

                    
        
        